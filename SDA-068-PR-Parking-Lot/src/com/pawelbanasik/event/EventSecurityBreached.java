package com.pawelbanasik.event;

import java.util.List;

import com.pawelbanasik.parking.interfaces.IGatesOpenEventListener;
import com.pawelbanasik.parking.interfaces.ISecurityBreachListener;

public class EventSecurityBreached implements IEvent {

	@Override
	public void execute() {
		List<ISecurityBreachListener> listeners = (List<ISecurityBreachListener>) EventDispatcher.INSTANCE
				.getAllObjectsImplementingInterface(ISecurityBreachListener.class);

		for (ISecurityBreachListener listener : listeners) {
			listener.securityBreached();
		}
	}
}
