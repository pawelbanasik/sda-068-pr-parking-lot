package com.pawelbanasik.event;

import java.util.List;

import com.pawelbanasik.parking.interfaces.IGatesOpenEventListener;

public class EventOpenGates implements IEvent {

	@Override
	public void execute() {
		// obsługa zdarzenia
		List<IGatesOpenEventListener> listeners = (List<IGatesOpenEventListener>) EventDispatcher.INSTANCE
				.getAllObjectsImplementingInterface(IGatesOpenEventListener.class);

		for (IGatesOpenEventListener listener : listeners) {
			listener.gatesOpen();
		}

	}
}
