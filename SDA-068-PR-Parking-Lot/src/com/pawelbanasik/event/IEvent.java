package com.pawelbanasik.event;

public interface IEvent {
	void execute();
}
