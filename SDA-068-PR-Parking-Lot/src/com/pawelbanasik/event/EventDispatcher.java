package com.pawelbanasik.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.ClassUtils;

public enum EventDispatcher {
	INSTANCE;

	private Map<Class<?>, List<Object>> map = new HashMap<>();
	private ExecutorService executors = Executors.newCachedThreadPool();

	public void registerObject(Object o) {
		// pobieram listę interfejsów które implemetuje klasa
		List<Class<?>> interfacesImplementedByObject = ClassUtils.getAllInterfaces(o.getClass());

		// dla każdego z tych interfejsów dodaję go do listy obiektów je
		// implementujących
		for (Class<?> classtype : interfacesImplementedByObject) {
			List<Object> objects = map.get(classtype);
			if (objects == null) { // jeśli lista nie istnieje (nie ma obiektu
									// który implementuje ten interfejs)
				objects = new ArrayList<>(); // tworze nowa liste
			}
			objects.add(o); // dodaje obiekt do listy
			map.put(classtype, objects); // umieszczam listę z powrotem w mapie
		}
	}

	public List getAllObjectsImplementingInterface(Class<?> implementedInterface) {
		return map.get(implementedInterface);
	}
	
	public void dispatchEvent(final IEvent event){
		executors.submit(new Runnable(){
			@Override
			public void run() {
				event.execute();
			}
		});
	}
}
