package com.pawelbanasik.parking.interfaces;

public interface Rate {

	public static enum RateType {
		WEEK, NIGHT, FREE
	};

	public static double countRate(RateType type) {
		switch (type) {
		case FREE:
			return 0.0;
		case NIGHT:
			return 0.5;
		case WEEK:
		default:
			return 3.0;
		}
	}
}
