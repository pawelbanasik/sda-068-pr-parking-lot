package com.pawelbanasik.parking.interfaces;

public interface IRate {
	double getRate();
}
