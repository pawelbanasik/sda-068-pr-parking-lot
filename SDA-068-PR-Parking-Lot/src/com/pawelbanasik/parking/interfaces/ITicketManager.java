package com.pawelbanasik.parking.interfaces;

import java.util.Optional;

import com.pawelbanasik.parking.model.Gate;
import com.pawelbanasik.parking.model.Ticket;

public interface ITicketManager {

	Optional<Ticket> tryGenerateTicket(Gate gate, String reg);
	boolean tryValidate(Gate gate, int ticketId, String reg);
	void validate(Gate gate, int ticketId);
}
