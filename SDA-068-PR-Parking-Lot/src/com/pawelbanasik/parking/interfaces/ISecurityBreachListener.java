package com.pawelbanasik.parking.interfaces;

public interface ISecurityBreachListener {

	void securityBreached();
}
