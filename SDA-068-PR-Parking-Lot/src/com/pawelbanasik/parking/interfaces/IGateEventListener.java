package com.pawelbanasik.parking.interfaces;

public interface IGateEventListener {
	void in(int ticketId, String reg);
	void out(int ticketId, String reg);
}
