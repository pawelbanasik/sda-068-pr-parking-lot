package com.pawelbanasik.parking.interfaces;

public interface IGatesOpenEventListener {

	void gatesOpen();
}
