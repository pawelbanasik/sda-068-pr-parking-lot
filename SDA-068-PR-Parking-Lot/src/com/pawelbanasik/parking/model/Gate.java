package com.pawelbanasik.parking.model;

import java.util.Optional;

import com.pawelbanasik.event.EventDispatcher;
import com.pawelbanasik.parking.interfaces.IGatesOpenEventListener;
import com.pawelbanasik.parking.interfaces.ITicketManager;

public class Gate implements IGatesOpenEventListener {

	private int id;
	private boolean isOpen;
	private ITicketManager ticketProvider;

	public Gate(int id, ITicketManager ticketProvider) {
		super();
		this.id = id;
		this.isOpen = true;
		this.ticketProvider = ticketProvider;
		
		EventDispatcher.INSTANCE.registerObject(this);
	}

	@Override
	public void gatesOpen() {
		System.out.println("Gate " + id + " is now opening.");
	}

	private Optional<Ticket> generateTicket(String registrationNumber) {
		return ticketProvider.tryGenerateTicket(this, registrationNumber);
	}

	public boolean checkIn(String registrationNumber) {
		if (!isOpen) {
			return false;
		}

		Optional<Ticket> generatedTicket = generateTicket(registrationNumber);
		if (generatedTicket.isPresent()) {
			System.out.println(generatedTicket.get());
			return true;
		}
		return false;
	}

	public boolean checkOut(int ticketId, String registrationNumber) {
		if (!ticketProvider.tryValidate(this, ticketId, registrationNumber)) {
			ticketProvider.validate(this, ticketId);
		}
		return true;
	}

	public int getId() {
		return id;
	}
}
