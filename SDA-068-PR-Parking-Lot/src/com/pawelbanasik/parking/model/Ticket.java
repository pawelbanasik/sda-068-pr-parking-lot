package com.pawelbanasik.parking.model;

public class Ticket {
	private static int counter = 0;
	private int id;
	private long timestampIn;
	private long timestampOut;
	private long timestampPay;
	private boolean paid;
	private double amountToPay;
	private double ticketRate;

	public Ticket(double rate) {
		super();
		// this.id = (int)(System.currentTimeMillis()/1000);
		this.id = counter++;
		this.timestampIn = System.currentTimeMillis();
		this.timestampOut = 0;
		this.timestampPay = 0;
		this.paid = false;
		this.amountToPay = 0.0;
		this.ticketRate = rate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTimestampIn() {
		return timestampIn;
	}

	public void setTimestampIn(long timestampIn) {
		this.timestampIn = timestampIn;
	}

	public long getTimestampOut() {
		return timestampOut;
	}

	public void setTimestampOut(long timestampOut) {
		this.timestampOut = timestampOut;
	}

	public long getTimestampPay() {
		return timestampPay;
	}

	public void setTimestampPay(long timestampPay) {
		this.timestampPay = timestampPay;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(double amountToPay) {
		this.amountToPay = amountToPay;
	}

	public double getTicketRate() {
		return ticketRate;
	}

	public void setTicketRate(double converterInfo) {
		this.ticketRate = converterInfo;
	}

	public long getStayLength() {
		return System.currentTimeMillis() - timestampIn;
	}

}
