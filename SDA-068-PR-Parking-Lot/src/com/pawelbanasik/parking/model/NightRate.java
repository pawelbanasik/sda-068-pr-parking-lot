package com.pawelbanasik.parking.model;

import com.pawelbanasik.parking.interfaces.IRate;

public class NightRate implements IRate {
	@Override
	public double getRate() {
		return 0.5;
	}
}
