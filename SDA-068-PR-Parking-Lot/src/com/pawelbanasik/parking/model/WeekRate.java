package com.pawelbanasik.parking.model;

import com.pawelbanasik.parking.interfaces.IRate;

public class WeekRate implements IRate {

	@Override
	public double getRate() {
		return 3.0;
	}
}
