package com.pawelbanasik.parking.model;

import java.util.HashMap;
import java.util.Map;

import com.pawelbanasik.event.EventDispatcher;
import com.pawelbanasik.event.IEvent;
import com.pawelbanasik.parking.interfaces.IGatesOpenEventListener;

public class ParkingInfo implements IGatesOpenEventListener{
	private int capacity;
	private Map<Integer, Ticket> tickets = new HashMap<>();
	// alternatywnie (z treści) lista ticketów.
	// dzięki zmianie na mapę w metodzie getTicketInfo nie muszę przeszukiwać
	// kolekcji.

	public ParkingInfo(int capacity) {
		super();
		this.capacity = capacity;
		
		EventDispatcher.INSTANCE.registerObject(this);
	}
	
	@Override
	public void gatesOpen() {
System.err.println("Jestem parkingiem i mnie to nie interesuje.");
		
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public boolean hasFreeSpot() {
		return tickets.size() < capacity;
	}

	public Ticket getTicketInfo(int ticketId) {
		return tickets.get(ticketId);
	}

	public void addTicket(Ticket t) {
		tickets.put(t.getId(), t);
	}
}
