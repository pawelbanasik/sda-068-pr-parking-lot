package com.pawelbanasik.parking.model;

import com.pawelbanasik.event.EventDispatcher;
import com.pawelbanasik.parking.interfaces.ISecurityBreachListener;

public class SecurityGate implements ISecurityBreachListener {

	public SecurityGate() {
		EventDispatcher.INSTANCE.registerObject(this);
	}

	@Override
	public void securityBreached() {
		System.err.println("Już tam śmigam!");
	}
}
