package com.pawelbanasik;

import com.pawelbanasik.parking.model.CarDatabase;
import com.pawelbanasik.parking.model.SecurityGate;

public class Olivia {
	private GateServer serverGates;
	private CarDatabase carDb;

	public Olivia() {
		new SecurityGate();

		carDb = new CarDatabase();
		serverGates = new GateServer(carDb);
	}

	public GateServer getServerGates() {
		return serverGates;
	}

	public void setServerGates(GateServer serverGates) {
		this.serverGates = serverGates;
	}

	public void printCars() {
		carDb.printAll();
	}
}
