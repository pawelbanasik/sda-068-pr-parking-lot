package com.pawelbanasik;

import java.util.Optional;
import java.util.Scanner;

import javax.swing.text.html.Option;

import com.pawelbanasik.event.EventDispatcher;
import com.pawelbanasik.event.EventOpenGates;
import com.pawelbanasik.event.EventSecurityBreached;
import com.pawelbanasik.parking.interfaces.IRate;
import com.pawelbanasik.parking.model.FreeRate;
import com.pawelbanasik.parking.model.NightRate;
import com.pawelbanasik.parking.model.Ticket;
import com.pawelbanasik.parking.model.WeekRate;

public class Main {

	public static void main(String[] args) {
		Olivia o = new Olivia();

		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] splits = line.split(" ");
			try {
				if (splits[0].equals("addParking")) {
					addParking(o, splits[1], splits[2]);
				} else if (splits[0].equals("addGate")) {
					addGate(o, splits[1], splits[2]);
				} else if (splits[0].equals("checkOut")) {
					checkOut(o, splits[1], splits[2], splits[3]);
				} else if (splits[0].equals("balance")) {
					balance(o, splits[1], splits[2]);
				} else if (splits[0].equals("validate")) {
					validate(o, splits[1], splits[2], splits[3]);
				} else if (splits[0].equals("checkIn")) {
					checkIn(o, splits[1], splits[2]);
				} else if (splits[0].equals("addRoom")) {
					addRoom(o, splits[1]);
				} else if (splits[0].equals("buyRoom")) {
					buyRoom(o, splits[1], splits[2]);
				} else if (splits[0].equals("triggerAlarm")) {
					triggerAlarm(o, splits[1], splits[2]);
				} else if (splits[0].equals("openGates")) {
					openGates(o);
				} else if (splits[0].equals("closeGates")) {
					closeGates(o);
				} else if (splits[0].equals("changeGateRates")) {
					changeGateRates(o, splits[1]);
				}else if (splits[0].equals("cars")) {
					printCars(o);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				System.err.println("Niepoprawna ilość parametrów");
			}
		}
	}

	private static void printCars(Olivia o) {
		o.printCars();
	}

	private static void changeGateRates(Olivia o, String typ) {
		IRate rate = null;
		if (typ.equals("FREE")) {
			rate = new FreeRate();
		} else if (typ.equals("WEEK")) {
			rate = new WeekRate();
		} else if (typ.equals("NIGHT")) {
			rate = new NightRate();
		} else {
			System.err.println("Niepoprawny rate");
			return;
		}
		o.getServerGates().setRate(rate);
	}

	private static void openGates(Olivia o) {
		// trigger open gates
		EventDispatcher.INSTANCE.dispatchEvent(new EventOpenGates());
//		EventDispatcher.INSTANCE.dispatchEvent(new EventSecurityBreached());
	}

	private static void closeGates(Olivia o) {
		// trigger open gates
	}

	private static void addParking(Olivia o, String capacity, String parkingName) {
		try {
			int cap = Integer.parseInt(capacity);
			o.getServerGates().addParking(parkingName, cap);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr capacity");
		}
	}

	private static void addGate(Olivia o, String gate, String parkingName) {
		try {
			int gateId = Integer.parseInt(gate);
			o.getServerGates().addGate(gateId, parkingName);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr gate");
		}
	}

	private static void checkOut(Olivia o, String gateNumber, String registrationNumber, String ticketId) {
		try {
			int gateId = Integer.parseInt(gateNumber);
			int ticket = Integer.parseInt(ticketId);
			o.getServerGates().getGate(gateId).checkOut(ticket, registrationNumber);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr");
		}
	}

	private static void checkIn(Olivia o, String gateNumber, String registrationNumber) {
		try {
			int gateId = Integer.parseInt(gateNumber);
			o.getServerGates().getGate(gateId).checkIn(registrationNumber);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr gateId");
		}
	}

	private static void triggerAlarm(Olivia o, String string, String string2) {
		// TODO Auto-generated method stub

	}
	/////////////////////////////////// UNUSED YET

	private static void validate(Olivia o, String cash, String ticketId, String cashAm) {
		try {
			int cashId = Integer.parseInt(cash);
			int ticket = Integer.parseInt(ticketId);
			double cashAmount = Double.parseDouble(cashAm);
			// o.getCashServer().getCash(cashId).validate(ticket, cashAmount);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr");
		}
	}

	private static void balance(Olivia o, String cash, String ticketId) {
		try {
			int cashId = Integer.parseInt(cash);
			int ticket = Integer.parseInt(ticketId);
			// o.getCashServer().getCash(cashId).balance(ticket);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr");
		}
	}

	private static void addRoom(Olivia o, String room) {
		try {
			int roomId = Integer.parseInt(room);
			// o.getRoomsServer().addRoom(roomId);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr room");
		}
	}

	private static void buyRoom(Olivia o, String room, String payAmount) {
		try {
			int roomId = Integer.parseInt(room);
			// o.getRoomsServer().addRoom(roomId);
		} catch (NumberFormatException e) {
			System.err.println("Niepoprawny parametr room");
		}
	}
}
