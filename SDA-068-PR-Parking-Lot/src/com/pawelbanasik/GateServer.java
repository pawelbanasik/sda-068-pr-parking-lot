package com.pawelbanasik;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.pawelbanasik.parking.interfaces.IGateEventListener;
import com.pawelbanasik.parking.interfaces.IRate;
import com.pawelbanasik.parking.interfaces.ITicketManager;
import com.pawelbanasik.parking.interfaces.Rate;
import com.pawelbanasik.parking.interfaces.Rate.RateType;
import com.pawelbanasik.parking.model.CarDatabase;
import com.pawelbanasik.parking.model.FreeRate;
import com.pawelbanasik.parking.model.Gate;
import com.pawelbanasik.parking.model.ParkingInfo;
import com.pawelbanasik.parking.model.Ticket;
import com.pawelbanasik.parking.model.WeekRate;

public class GateServer implements ITicketManager {

	// private RateType currentRate;
	private IRate currentRate = new FreeRate();
	private IGateEventListener gateEventListener;

	private Map<String, ParkingInfo> parkings = new HashMap<>();
	private Map<Gate, ParkingInfo> gatesToParking = new HashMap<>();

	public GateServer(IGateEventListener gateEventListener) {
		super();
		this.gateEventListener = gateEventListener;
	}

	public void addParking(String name, int capacity) {
		System.out.println("Parking " + name + " added");
		parkings.put(name, new ParkingInfo(capacity));
	}

	public void addGate(int gateId, String parkingName) {
		if (parkings.containsKey(parkingName)) {
			gatesToParking.put(new Gate(gateId, this), parkings.get(parkingName));
			System.out.println("Gate " + gateId + " added");
		} else {
			System.out.println("Gate NOT " + gateId + " added");
		}
	}

	@Override
	public Optional<Ticket> tryGenerateTicket(Gate gate, String reg) {
		ParkingInfo parking = gatesToParking.get(gate);
		if (parking.hasFreeSpot()) {
			Ticket t = new Ticket(currentRate.getRate());
			// Ticket t = new Ticket(Rate.countRate(currentRate)); //
			// currentRate to enum
			parking.addTicket(t);
			System.out.println("Ticket " + t.getId() + " generated");

			gateEventListener.in(t.getId(), reg);
			return Optional.of(t);
		}
		System.out.println("Unable to generate ticket");
		return Optional.empty();
	}

	public void setRate(IRate rate) {
		this.currentRate = rate;
	}

	@Override
	public boolean tryValidate(Gate gate, int ticketId, String reg) {
		System.out.println("try Validating");

		ParkingInfo parking = gatesToParking.get(gate);
		Ticket toValidate = parking.getTicketInfo(ticketId);
		if(toValidate == null){
			return false;
		}
		if (!toValidate.isPaid()) {
			toValidate.setTimestampPay(System.currentTimeMillis());
			long stayLength = toValidate.getStayLength();
			double payAmount = stayLength * toValidate.getTicketRate();

			gateEventListener.out(ticketId, reg);
			
			System.out.println("Amount to pay is: " + payAmount);
			return payAmount == 0.0;
		}
		return true;
	}

	// tylko do testowania
	public Gate getGate(int gateNumber) {
		for (Gate g : gatesToParking.keySet()) {
			if (g.getId() == gateNumber) {
				return g;
			}
		}
		return null;
	}

	@Override
	public void validate(Gate gate, int ticketId) {
		ParkingInfo parking = gatesToParking.get(gate);
		Ticket toValidate = parking.getTicketInfo(ticketId);
		toValidate.setTimestampOut(System.currentTimeMillis());
		if (!toValidate.isPaid()) {
			long stayLength = toValidate.getStayLength();
			double payAmount = stayLength * toValidate.getTicketRate();

			toValidate.setTimestampPay(System.currentTimeMillis());
			toValidate.setPaid(true);
			toValidate.setAmountToPay(payAmount);
		}
	}
}
